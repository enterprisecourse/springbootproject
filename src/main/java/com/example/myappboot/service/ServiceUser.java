package com.example.myappboot.service;

import com.example.myappboot.entiti.Subscription;
import com.example.myappboot.entiti.User;
import com.example.myappboot.repository.DataUser;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Slf4j
@Component
public class ServiceUser implements Service {

    @Autowired
    DataUser dataUser;


    @SneakyThrows
    public void registrationService(User user) {

        List<User> userList = dataUser.checkUserDataBase(user.getLogin());

        if (userList.size() > 0) {
            throw new Exception("Пользователь с таким логином уже есть");
        }
        {
            dataUser.registrationData(user);
        }
    }

    public void checkUserDataBase(String login) throws Exception {
        boolean checkStatus = true;
        List<User> userList = dataUser.checkUserDataBase(login);

        if (userList.size() == 0) {
            throw new Exception("Пользователь с таким логином нет в базе, пожалуйста пройдите регистрацию в системе");
        }

    }

    public void addSubscriptionDataBase(Subscription subscription,String idUser) {
        dataUser.addSubscriptionDataBase(subscription,idUser);
    }

    public void addSubscriptionAndUserDataBase(Subscription subscription, String log) {
        dataUser.addSubscriptionAndUserDataBase(subscription, log);
    }

    public String getSubscriptionForLogin(String login) {
        return dataUser.getSubscriptionForLogin(login);
    }

    public String findSubscriptionForUser(String idUser) {
        return dataUser.findSubscriptionForUser(idUser);
    }

    public List<Subscription> findSubscriptionForidUserSubsc(String idUserSubscription) {
        return dataUser.findSubscriptionForidUserSubsc(idUserSubscription);
    }

    public void noticeEntryUser(Date dateEnterStart, Integer time, String idUserSubscription, String idUser, String dateEnterStartLong) {
        dataUser.noticeEntryUser(dateEnterStart, time, idUserSubscription, idUser, dateEnterStartLong);
    }

    public String getdataStart(String idUser) {

        return dataUser.getdataStart(idUser);
    }

    public int getOverallTime(String idUser) {
        return dataUser.getOverallTime(idUser);
    }

    public boolean checkTime(int overallTime, double minutes) {

        if (minutes > overallTime) {
            return false;
        }

        return true;

    }

    public void updateTablevisit(String idUser, String dateEnterExistLong, Date dateEnterFinish, int timefinal) {

        dataUser.updateTablevisit(idUser, dateEnterExistLong, dateEnterFinish, timefinal);
    }

    public String sessionDistributor(String IdUser) throws Exception {

        return dataUser.sessionDistributor(IdUser);


    }

    public void deleteTable(String idUser) {
        dataUser.deleteTable(idUser);
    }

    public List<Subscription> findSubscriptionForidUse(String idUser){
        return dataUser.findSubscriptionForidUse(idUser);
    }


}
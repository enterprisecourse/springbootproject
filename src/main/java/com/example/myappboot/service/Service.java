package com.example.myappboot.service;

import com.example.myappboot.entiti.Subscription;
import com.example.myappboot.entiti.User;

import java.util.Date;
import java.util.List;

public interface Service {


    public void registrationService(User user);

    public void checkUserDataBase(String login) throws Exception ;

    public void addSubscriptionDataBase(Subscription subscription,String idUser);

    public void addSubscriptionAndUserDataBase(Subscription subscription, String log);

    public String getSubscriptionForLogin(String login);

    public String findSubscriptionForUser(String idUser);

    public List<Subscription> findSubscriptionForidUserSubsc(String idUserSubscription);

    public void noticeEntryUser(Date dateEnterStart, Integer time, String idUserSubscription, String idUser, String dateEnterStartLong);

    public String getdataStart(String idUser);

    public int getOverallTime(String idUser);

    public boolean checkTime(int overallTime, double minutes);

    public void updateTablevisit(String idUser, String dateEnterExistLong, Date dateEnterFinish, int timefinal);

    public String sessionDistributor(String IdUser) throws Exception;

    public void deleteTable(String idUser);

    public List<Subscription> findSubscriptionForidUse(String idUser);

























}

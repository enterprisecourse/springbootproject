package com.example.myappboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyAppBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyAppBootApplication.class, args);
    }

}

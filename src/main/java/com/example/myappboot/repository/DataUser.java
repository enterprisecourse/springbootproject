package com.example.myappboot.repository;

import com.example.myappboot.entiti.Subscription;
import com.example.myappboot.entiti.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Slf4j
@Repository
@RequiredArgsConstructor
@Component
public class DataUser implements Data {

    private final JdbcTemplate source;

    private String sql = "INSERT INTO users (user_name,user_password,user_entryData) VALUES (?,?,?)"; // запрос для добавдение пользователя
    private String sql1 = "SELECT user_name,user_password,user_entryData" +
            " FROM users WHERE user_name LIKE ?"; // запрос для получение пользователя по логину
    private String string2 = "INSERT INTO subscription" +
            " (subscription_name,subscription_time,subscription_price,subscription_datepurchase,id_users) VALUES"; // вставляем абонемент в таблицу
    private String string1 = "INSERT INTO userssubscription" +
            " (userssubscription_purchase_date,userssubscription_state,id_users,id_subscription)\n" +
            "VALUES (?,?,(SELECT id_users FROM users WHERE user_name=?)," +
            "(SELECT id_subscription FROM subscription WHERE subscription_datepurchase=?));";    // вставляем пользователя и абонемент
    private String sql3 = "SELECT id_users FROM users WHERE user_name=?;\n";
    private String sql4 = "SELECT id_subscription FROM userssubscription WHERE id_users=?;\n";
    private String sql5 = "SELECT subscription_name,subscription_time,subscription_price,subscription_datepurchase" +
            " FROM subscription WHERE id_subscription=?;\n";
    private String sql6 = "SELECT date_exite FROM visit WHERE id_users=?;\n";
    private String sql7 = "SELECT date_entry FROM visit WHERE id_users=?;\n";
    private String sql9 = "INSERT INTO visit (date_entry,overall_time,id_subscription,id_users,date_entrylong) VALUES (?,?,?,?,?)";
    private String sql10 = "SELECT date_entrylong FROM visit WHERE id_users=?;\n";
    private String sql11 = "SELECT overall_time FROM visit WHERE id_users=?;\n";
    private String sql12 = "UPDATE visit SET overall_time = ?,date_exite=?,date_exitelong=? WHERE id_users = ?;";
    private String sql13 = "DELETE FROM visit WHERE id_users=?";

    public void registrationData(User user) {
        String login = user.getLogin();
        String password = user.getPassword();
        String dateRegistraction = user.getEntryData();
        source.update(sql, login, password, dateRegistraction);
    } // регистрация пользователя

    public List<User> checkUserDataBase(String login) {
        return source.query(sql1,
                new Object[]{login},
                (rs, i) -> {
                    return new User()
                            .setLogin(rs.getString("user_name"))
                            .setPassword(rs.getString("user_password"))
                            .setEntryData(rs.getString("user_entryData"));
                });
    }

    public void addSubscriptionDataBase(Subscription subscription,String idUser) {
        int IDUser = Integer.parseInt(idUser);
        source.update(string2 + "\n" +
                "(?,?,?,?,?)", subscription.getNames(), subscription.getTime(), subscription.getPrice(), subscription.getDatePurchase(),IDUser);
    }

    public void addSubscriptionAndUserDataBase(Subscription subscription, String log) {
        source.update(string1, subscription.getDatePurchase(), "active", log, subscription.getDatePurchase());
    }

    public String getSubscriptionForLogin(String login) {
        List<String> id = source.query(sql3,
                new Object[]{login},
                (rs, i) -> {
                    return rs.getString("id_users");
                });

        return id.get(0);
    }

    public String findSubscriptionForUser(String idUser) {
        int ID = Integer.parseInt(idUser);
        List<String> idSub = source.query(sql4,
                new Object[]{ID},
                (rs, i) -> {
                    return rs.getString("id_subscription");
                });
        if(idSub.size()==0){return "0";}else {return idSub.get(0);}

    }

    public List<Subscription> findSubscriptionForidUserSubsc(String idUserSubscription) {

        int ID = Integer.parseInt(idUserSubscription);
        List<Subscription> subscriptionList = source.query(
                sql5,
                new Object[]{ID},

                (rs, i) -> {
                    return new Subscription()
                            .setNames(rs.getString("subscription_name"))
                            .setTime(rs.getInt("subscription_time"))
                            .setPrice(rs.getString("subscription_price"))
                            .setDatePurchase(rs.getString("subscription_datepurchase"));
                });
        return subscriptionList;
    }

    public String sessionDistributor(String idUser) {
        String statusCheck = null;
        List<String> idSessionsUserExit;
        List<String> idSessionsUserStart;
        int ID = Integer.parseInt(idUser);

        idSessionsUserExit = source.query(sql6,
                new Object[]{ID},
                (rs, i) -> {
                    return rs.getString("date_exite");
                });
        idSessionsUserStart = source.query(sql7,
                new Object[]{ID},
                (rs, i) -> {
                    return rs.getString("date_entry");
                });
        if (idSessionsUserStart.size() == 0 && idSessionsUserExit.size() == 0) {
            statusCheck = "Auth-no/Exit-no";
        } else {
            if (idSessionsUserStart.get(0) != null && idSessionsUserExit.get(0) == null) {
                statusCheck = "Auth-ok/Exit-no";
            }
            if (idSessionsUserStart.get(0) != null && idSessionsUserExit.get(0) != null) {
                statusCheck = "Auth-ok/Exit-ok";
            }
        }
        return statusCheck;
    }

    public void noticeEntryUser(Date dateEnterStart, Integer time, String idUserSubscription, String idUser, String dateEnterStartLong) {
        String dataEnry = String.valueOf(dateEnterStart);
        int idUserSub = Integer.parseInt(idUserSubscription);
        int idUs = Integer.parseInt(idUser);
        source.update(sql9
                , dataEnry, time, idUserSub, idUs, dateEnterStartLong);
    } // регистрация пользователя

    public String getdataStart(String idUser) {
        List<String> listTime;
        int ID = Integer.parseInt(idUser);
        listTime = source.query(sql10,
                new Object[]{ID},
                (rs, i) -> {
                    return rs.getString("date_entrylong");
                });
        return listTime.get(0);

    }

    public int getOverallTime(String idUser) {
        List<Integer> listOverall = null;
        int ID = Integer.parseInt(idUser);
        listOverall = source.query(sql11,
                new Object[]{ID},
                (rs, i) -> {
                    return rs.getInt("overall_time");
                });
        return listOverall.get(0);
    }

    public void updateTablevisit(String idUser, String dateEnterExistLong, Date dateEnterFinish, int timefinal) {
        String dataExist = dateEnterFinish.toString();
        int ID = Integer.parseInt(idUser);
        source.update(sql12,
                timefinal, dataExist, dateEnterExistLong, ID);
    }

    public void deleteTable(String idUser) {
        int ID = Integer.parseInt(idUser);
        source.update(sql13, ID);
    }

    public List<Subscription> findSubscriptionForidUse(String idUser) {

        int ID = Integer.parseInt(idUser);
        List<Subscription> subscriptionList = source.query(
                "SELECT subscription_name,subscription_time,subscription_price,subscription_datepurchase" +
                        " FROM subscription WHERE id_users=?;\n",
                new Object[]{ID},

                (rs, i) -> {
                    return new Subscription()
                            .setNames(rs.getString("subscription_name"))
                            .setTime(rs.getInt("subscription_time"))
                            .setPrice(rs.getString("subscription_price"))
                            .setDatePurchase(rs.getString("subscription_datepurchase"));
                });
        return subscriptionList;
    }








}





package com.example.myappboot.repository;

import com.example.myappboot.entiti.Subscription;
import com.example.myappboot.entiti.User;

import java.util.Date;
import java.util.List;

public interface Data {

    public void registrationData(User user);

    public List<User> checkUserDataBase(String login);

    public void addSubscriptionDataBase(Subscription subscription,String idUser);

    public void addSubscriptionAndUserDataBase(Subscription subscription, String log);

    public String getSubscriptionForLogin(String login);

    public String findSubscriptionForUser(String idUser);

    public List<Subscription> findSubscriptionForidUserSubsc(String idUserSubscription);

    public String sessionDistributor(String idUser);

    public void noticeEntryUser(Date dateEnterStart, Integer time, String idUserSubscription, String idUser, String dateEnterStartLong);

    public String getdataStart(String idUser);

    public int getOverallTime(String idUser);

    public void updateTablevisit(String idUser, String dateEnterExistLong, Date dateEnterFinish, int timefinal);

    public void deleteTable(String idUser);

    public List<Subscription> findSubscriptionForidUse(String idUser);






























}

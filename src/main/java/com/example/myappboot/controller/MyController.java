package com.example.myappboot.controller;
import com.example.myappboot.entiti.SessionApp;
import com.example.myappboot.entiti.Subscription;
import com.example.myappboot.entiti.User;
import com.example.myappboot.service.ServiceUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequiredArgsConstructor
public class MyController {

    @Autowired
    ServiceUser serviceUser;

    @Autowired
    private SessionApp sessionApp;
    static Date dateEnterStart;
    static Date dateEnterFinish;

    public Map<String, Subscription> getSubscriptionMap() {
        Map<String, Subscription> subscriptionMap = new HashMap();
        subscriptionMap.put("1", new Subscription().setNames("Subscription 100 hours").setPrice("100").setTime(100));
        return subscriptionMap;
    } // наши абонементы

    @PostMapping("buySubscription") // API покупки абонемента
    public void mainApp(@RequestParam("login") String login, @RequestParam("IdSubscription") String IdSubscription)
            throws Exception {

        String login1 = login;
        String IdSubscript = IdSubscription;
        serviceUser.checkUserDataBase(login1); // проверяем есть ли такой у нас пользователь воообще в нашей БД

        Subscription subscription = getSubscriptionMap().get(IdSubscript); //достаём абонемент который выбрал юзер
        subscription.setDatePurchase(new Date().toString()); // добавляем дату покупки абонемента

        String idUser = serviceUser.getSubscriptionForLogin(login); // получили ID юзера для поиска абонемента

        List<Subscription> listSubsUser = serviceUser.findSubscriptionForidUse(idUser); // находим все абонементы клиента

        for (Subscription subscription1 : listSubsUser) {
            if(subscription1.getNames().equals(subscription.getNames())){
                throw new Exception("такой абонемент есть");
            }
        }

        serviceUser.addSubscriptionDataBase(subscription,idUser);
        serviceUser.addSubscriptionAndUserDataBase(subscription,login1); // добавляем абонемент и юзера в таблицу userssubscription

        }

    @PostMapping("getSubscription") // API запроса для отображения абонементов
    public Map<String, Subscription> getSubscription(@RequestParam("login") String login)throws Exception {

        Map<String, Subscription> subscriptionNEW = new HashMap();
        String idUser = serviceUser.getSubscriptionForLogin(login); // получили ID юзера для поиска абонемента
        Map<String, Subscription> subscriptionMNew= getSubscriptionMap();
        List<Subscription> listSubsUser = serviceUser.findSubscriptionForidUse(idUser); // находим все абонементы клиента


        if(listSubsUser.size()>0){

            if(listSubsUser.size()==2){return subscriptionNEW;}

            List<Subscription> list = new ArrayList<Subscription>(subscriptionMNew.values());

            for(int i=0;i<list.size();i++){

                System.out.println("---------------------------------------");
                System.out.println("list.get(i) "+list.get(i));

                for(int j=0;j<listSubsUser.size();j++){

                    Subscription subscription = listSubsUser.get(j).setDatePurchase(null);
                    Subscription subscription1 = list.get(i);

                    if(!subscription.getNames().equals(subscription1.getNames())){
                        subscriptionNEW.put(String.valueOf(j+1),subscription1);
                    }

                }
            }
            return subscriptionNEW;
        }


        return subscriptionMNew;
    }


    @PostMapping("addUser") // API добавить пользователя
    public void LoginАpplicationApp(@RequestParam("login") String login, @RequestParam("Password") String password)
            throws Exception {
        User user = new User().setLogin(login).setPassword(password).setEntryData(new Date().toString());
        serviceUser.registrationService(user);
    }

    @PostMapping("entryApp") // API вход в приложение
    public void entryAppMob(@RequestParam("login") String login, @RequestParam("Password") String password)
            throws Exception {

        dateEnterStart = sessionApp.GetTime(); // получили дату входа в приложения
        long dateEnterStartLong = dateEnterStart.getTime();  // преобразовали для работы со временем

        String idUser = serviceUser.getSubscriptionForLogin(login); // получили ID юзера для поиска абонемента

        String sessionDistributor = serviceUser.sessionDistributor(idUser); // проверяем есть ли у пользователя не завершенная ссесия

        if (sessionDistributor.equals("Auth-ok/Exit-no")) {
            log.error("Просим завершить ссесию в приложении");
        } // логика когда у клиента есть вход но нет выхода

        if (sessionDistributor.equals("Auth-no/Exit-no")) {
            String idUserSubscription = serviceUser.findSubscriptionForUser(idUser);  // находим ID абонемента
            List<Subscription> subscriptionList = serviceUser.findSubscriptionForidUserSubsc(idUserSubscription); // находим абонемент
            Subscription subscription = subscriptionList.get(0);
            User user = new User().setLogin(login).setPassword(password).setEntryData(new Date().toString()); // определяем пользователя
            serviceUser.noticeEntryUser(dateEnterStart, subscription.getTime(), idUserSubscription, idUser, String.valueOf(dateEnterStartLong));
        } // логика когда у клиента не было авторизаций вообще

        if (sessionDistributor.equals("Auth-ok/Exit-ok")) {

            String idUserSubscription = serviceUser.findSubscriptionForUser(idUser);  // находим ID абонемента
            List<Subscription> subscriptionList = serviceUser.findSubscriptionForidUserSubsc(idUserSubscription); // находим абонемент
            Subscription subscription = subscriptionList.get(0);
            User user = new User().setLogin(login).setPassword(password).setEntryData(new Date().toString()); // определяем пользователя

            int overallTime = serviceUser.getOverallTime(idUser); // получаем время из таблицы
            serviceUser.deleteTable(idUser);
            serviceUser.noticeEntryUser(dateEnterStart, subscription.getTime(), idUserSubscription, idUser, String.valueOf(dateEnterStartLong));

        } // логика когда у клиента была авторизация ранее
    } // войти в приложение

    @PostMapping("exiteApp") // API выход из приложение
    public void exiteApp(@RequestParam("login") String login, @RequestParam("Password") String password)
            throws Exception {

        dateEnterFinish = sessionApp.GetTime(); // получили дату выхода из приложения

        String idUser = serviceUser.getSubscriptionForLogin(login); // получили ID юзера для поиска абонемента

        String sessionDistributor = serviceUser.sessionDistributor(idUser); // проверяем есть ли у пользователя не завершенная ссесия

        if (sessionDistributor.equals("Auth-no/Exit-no")) {
            log.error("Просим пройти авторизацию");
        } // когда у клиента не было авторизаций

        if (sessionDistributor.equals("Auth-ok/Exit-no")) {


            long dateEnter = dateEnterFinish.getTime();
            String idUserSubscription = serviceUser.findSubscriptionForUser(idUser);  // находим ID абонемента
            List<Subscription> subscriptionList = serviceUser.findSubscriptionForidUserSubsc(idUserSubscription); // находим абонемент
            Subscription subscription = subscriptionList.get(0);

            dateEnterStart = new Date(Long.valueOf(serviceUser.getdataStart(idUser))); // получаем время входа в приложения

            System.out.println("dateEnterStart "+dateEnterStart);
            System.out.println("dateEnterFinish "+dateEnterFinish);

            double minutes = ((dateEnterFinish.getTime() - dateEnterStart.getTime()) / 1000) / 60;  // получаем сколько времени прошло с момента входа до выхода
            System.out.println("minutes "+minutes);
            int overallTime = serviceUser.getOverallTime(idUser); // получаем время из таблицы

            if (serviceUser.checkTime(overallTime, minutes) == true) {
                long dateEnterExist = dateEnterStart.getTime();
                int Timefinal = (int) (overallTime - minutes);
                User user = new User().setLogin(login).setPassword(password).setEntryData(new Date().toString()); // определяем пользователя
                serviceUser.updateTablevisit(idUser, String.valueOf(dateEnter), dateEnterFinish, Timefinal);

            }

        } // когда у клиента есть авторизация но нет завершения

        if (sessionDistributor.equals("Auth-ok/Exit-ok")) {
            log.error("Просим пройти авторизацию");
        } // когда у клиента были ранее авторизации



    }

}

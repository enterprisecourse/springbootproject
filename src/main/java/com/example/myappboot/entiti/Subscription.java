package com.example.myappboot.entiti;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Subscription {

    String names;
    Integer time;
    String price;
    String DatePurchase;

}
